section .text

;rdi - error code
exit:
	mov rax, 60		;sys_exit call code
	syscall





;rdi - str pointer
string_length:
	mov rax, rdi
.count:
	cmp byte [rax], 0
	je .end
	inc rax
	jmp .count
.end:
	sub rax, rdi
	ret





;rdi - str pointer
print_string:
	push rbp
	mov rbp, rsp
	push rdi
	
	call string_length
	mov rdx, rax
	pop rsi
	mov rax, 1
	mov rdi, 1
	syscall
	
	leave
	ret





print_newline:
	mov rdi, 0xA	;new_line_char
	jmp print_char
	
	
	
	
;rdi - char
print_char:
	push rbp
	mov rbp, rsp
	push rdi
	
	mov rsi, rsp
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	syscall
	
	leave
	ret
	



;rdi - 8b numb
print_uint:
	push rbp
	mov rbp, rsp
	
	push word 0		;push 0 to the end of the digit to make digit a null-term string
	mov r10, 10		;div by 10
	
    mov rax, rdi
.div:				;r8 - form two byte digits to put to the stack
	xor rdx, rdx
    div r10
    mov r8B, dl		;reminder to r8
    add r8, 0x30	;to ascii code
    shl r8, 8		;place for next digit
    test eax, eax
    je .stop_middle	;r8 is not on the stack
    
    xor rdx, rdx
    div r10
    mov r8B, dl		;reminder to r8
    add r8, '0'		;to ascii code
	push r8W		
    test eax, eax
    je .stop_end	;r8 is on the stack
    
    xor r8, r8
    jmp .div
    
.stop_middle:
	push r8W
	lea rdi, [rsp+1]
	call print_string
    jmp .end
    
.stop_end:
	mov rdi, rsp
	call print_string
.end:
	leave	
    ret





;rdi - singed 8b number
print_int:
	cmp rdi, 0
	jge .print_digit
	
	push rdi
	mov rdi, '-' 		
	call print_char
	pop rdi
	
	neg rdi
.print_digit:
	jmp print_uint
	



;rdi - current symbol pointer
parse_uint:
;r8 to read one digit

	xor rax, rax		;rax - accumulator
	xor rcx, rcx		;rcx - counter

	mov r10, 10			;mul by 10
	xor rdx, rdx		;to check mul result > 64bits

.DIGIT:
	mov r8B, [rdi]		;read next symb
	inc rdi
	
	cmp r8B, '0'
    jb .FINISH
    cmp r8B, '9'
    ja .FINISH
	
.parseDigit:
	and r8, 0x0f		;from ascii to number
	
	mul	r10				;if large go to ERROR
	cmp rdx, 0
	jne .ERROR
				
	add rax, r8
	jc .ERROR			;if large go to ERROR
	
	add rcx, 1
	jc .ERROR			;if too long go to ERROR
	
	jmp .DIGIT

.FINISH:
	mov rdx, rcx
	ret

.ERROR:
	xor rax, rax
	xor rdx, rdx
	ret




;rdi - current symbol pointer
parse_int:
	push rbp
	mov rbp, rsp
	
	mov r8B, [rdi]		;read next symb
	inc rdi
	
.minus:
    cmp r8B, '-'
    jne .plus
    push -1
    jmp .sign_parse
    
.plus:
	cmp r8B, '+'
    jne .else_parse
    push 1
    jmp .sign_parse
    
.sign_parse:
	call parse_uint
	cmp rdx, 0			;should contain digits and be parsed correct
	je .ERROR
	cmp rax, 0			;should be positive
	jl .ERROR
	
	add rdx, 1				
	jc .ERROR			;len shouldn't be too large
	
	pop r10				;restore sign-number
	cmp r10, 0
	jl .minus_finish
	
	mov rsp, rbp
	pop rbp
	ret
	
.minus_finish:
	neg rax	
	
	mov rsp, rbp
	pop rbp
	ret

.else_parse:
	dec rdi 			;restore rdi if it wasn't a sign
	call parse_uint
	
	cmp rdx, 0			;should contain digits and be parsed correct
	je .ERROR
	cmp rax, 0			;should be positive
	jl .ERROR

	leave
	ret

.ERROR:
	xor rax, rax
	xor rdx, rdx

	leave
	ret




;rdi - first string pointer
;rsi - second string pointer
string_equals:
.cmp:
    mov r8B, [rdi]		;first string char
    cmp r8B, [rsi]
    jne .not_eq
    
	cmp r8B, 0
	je .eq
	
	inc rdi
	inc rsi
	jmp .cmp
    
.eq:
	mov rax, 1
	ret

.not_eq:
	xor rax, rax
   	ret




;rdi - string pointer
;rsi - buffer pointer
;rdx - buffer length
string_copy:
	xor rax, rax
	cmp rdx, 0
	je .error
.copy:
	cmp rax, rdx
	je .stop
	
	mov r8B, [rdi]
	mov [rsi], r8B
	inc rax
	
	inc rdi
	inc rsi
	jmp .copy
  
.stop:
	cmp r8B, 0
	jne .error
    ret
    
.error:
	xor rax, rax
	ret


read_char:
	push rbp
	mov rbp, rsp
	
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp 
    mov rdx, 1
    
    syscall
    test rax, rax
    js .error
    
    pop rax
    leave
    ret 
    
.error:
	mov rax, -1
	leave
    ret  




;rdi - buffer pointer
;rsi - buffer length
read_word:
	push rbp
	mov rbp, rsp
	
	
	dec rsi 			;place for null-terminated string 	
	push rsi			;store buffer length

	push rdi

.SPACE:
    call read_char
  
    cmp al, ' '
    je .SPACE
    cmp al, `\t`
    je .SPACE
    cmp al, `\n`
    je .SPACE
    cmp al, 0
    je .STOP_EMPTY	
    
    mov rcx, [rsp+8] 	;check length is 0
    add rcx, 1
	jc .STOP_EMPTY		
    
    
    pop rdi				;current char
    xor rcx, rcx		;counter
   
    
    mov [rdi], al
    inc rdi
    inc rcx

.READ_NEXT:
	cmp rcx, [rsp]
	je .COUNT_STOP
	 
	push rdi
	push rcx 
	call read_char
	pop rcx
	pop rdi
    
    cmp al, ' '
    je .STOP
    cmp al, `\t`
    je .STOP
    cmp al, `\n`
    je .STOP
    cmp al, 0
    je .STOP
    
    
    mov [rdi], al
    inc rdi
    inc rcx
    jmp .READ_NEXT

.COUNT_STOP:
	mov al, [rdi-1]
	cmp al, ' '
    je .STOP
    cmp al, `\t`
    je .STOP
    cmp al, `\n`
    je .STOP
    cmp al, 0
    je .STOP
    
    jmp .ERROR 
		
.STOP:
	mov byte [rdi], 0
	
	mov rdx, rcx
	
	mov rax, rdi			;compute original buffer pointer
	sub rax, rcx	
	
	leave					
	ret

.STOP_EMPTY:
	mov rax, [rsp]
	xor rdx, rdx
	
	leave
	ret
	
.ERROR:
	xor rax, rax
	xor rdx, rdx
	
	leave
	ret
